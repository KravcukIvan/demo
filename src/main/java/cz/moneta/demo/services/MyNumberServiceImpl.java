package cz.moneta.demo.services;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Service for editing number based on a very secret algorithm
 * Included: editNumber()
 *
 * @author Ivan Kravčuk
 */

@Service
public class MyNumberServiceImpl implements MyNumberService {


    /**
     * Edit Number based on the secret algorithm
     *
     * @param number number to be edited
     * @return the edited number
     */
    @Override
    public Integer editNumber(Integer number) {

        List<Integer> digits = new ArrayList<>();
        int evenCount = 0;

        //separates digits in the number backwards
        while (number > 0) {
            digits.add(number % 10);
            number /= 10;
        }

        //moves digits that are <=3 one position to the right (left in this case)
        for (int i = 0; i < digits.size(); i++) {
            if (digits.get(i) <= 3 && i > 0)
                Collections.swap(digits, i - 1, i);
        }

        Collections.reverse(digits);

        //multiplies digits 8 and 9 by two, splits the result into two digits and counts even numbers
        for (int i = 0; i < digits.size(); i++) {
            if (digits.get(i) >= 8) {
                digits.set(i, digits.get(i) * 2);
                digits.set(i, digits.get(i) % 10);
                digits.add(i, 1);
                i++;
                evenCount++;
            } else if (digits.get(i) % 2 == 0)
                evenCount++;
        }

        //takes out 7 from the list and concatenates them back together
        Integer meta = digits.stream()
                //.map( x->(x==8 || x==9) ? x*2 : x )
                .filter(x -> x != 7)
                .reduce(0, (Integer x, Integer y) -> 10 * x + y);


        Integer out = meta / evenCount;

        return out;
    }
}
