package cz.moneta.demo.controllers;

import cz.moneta.demo.services.MyNumberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * RestController for editing number based on a very secret algorithm
 * Included: editNumber()
 *
 * @author Ivan Kravčuk
 */

@RestController
@RequestMapping(MyNumberController.BASE_URL)
public class MyNumberController {

    public static final String BASE_URL = "/api/numbers";

    @Autowired
    private final MyNumberService myNumberService;

    @Autowired
    public MyNumberController(MyNumberService myNumberService) {
        this.myNumberService = myNumberService;
    }

    /**
     * Edit number based on the secret algorithm
     * URI: /api/numbers/{number}
     * Method: GET
     *
     * @param number number to be edited
     * @return the edited number
     * @see cz.moneta.demo.services.MyNumberServiceImpl
     */
    @GetMapping(value = "/edit/{number}")
    Integer editNumber(@PathVariable(value = "number") Integer number) {
        return myNumberService.editNumber(number);
    }
}
