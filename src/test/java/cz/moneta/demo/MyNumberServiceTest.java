package cz.moneta.demo;

import cz.moneta.demo.services.MyNumberService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MyNumberServiceTest {

    @Autowired
    private MyNumberService myNumberService;


    @Test
    public void editNumberTest() {


        Integer test1 = myNumberService.editNumber(43256791);
        Integer test1a = 11331545;

        Integer test2 = myNumberService.editNumber(1242918);
        Integer test2a = 82436432;

        Assert.assertEquals(test1a, test1);
        Assert.assertEquals(test2a, test2);

    }


}
